cmake \
 -DAPPLICATION="LoRaMac" \
 -DSUB_PROJECT="periodic-uplink-lpp" \
 -DCLASSB_ENABLED="ON" \
 -DACTIVE_REGION="LORAMAC_REGION_AS923" \
 -DREGION_EU868="OFF" \
 -DREGION_US915="OFF" \
 -DREGION_CN779="OFF" \
 -DREGION_EU433="OFF" \
 -DREGION_AU915="OFF" \
 -DREGION_AS923="ON" \
 -DREGION_CN470="OFF" \
 -DREGION_KR920="OFF" \
 -DREGION_IN865="OFF" \
 -DREGION_RU864="OFF" \
 -DBOARD="PineDio-USB" \
 -DMBED_RADIO_SHIELD="sx126x" \
 -DSECURE_ELEMENT="SOFT_SE" ..

# echo spidev > /sys/class/spi_master/spi1/spi1.0/driver_override

# echo spi1.0 > /sys/bus/spi/drivers/spidev/bind

# for i in /sys/bus/usb/drivers/spi-ch341-usb/*/spi_master/spi*/spi*.*; do echo spidev > $i/driver_override; echo $(basename $i) > /sys/bus/spi/drivers/spidev/bind; done

# bash: echo: write error: Device or resource busy
