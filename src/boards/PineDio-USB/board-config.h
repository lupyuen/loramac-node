#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define RADIO_NSS PA_0
#define RADIO_BUSY PA_1
#define RADIO_DIO_1 PA_2
#define RADIO_DEVICE_SEL PA_3
#define RADIO_RESET PA_4
#define RADIO_ANT_SWITCH_POWER PA_5

/*!
 * Defines the time required for the TCXO to wakeup [ms].
 */
#define BOARD_TCXO_WAKEUP_TIME                      0

#ifdef __cplusplus
}
#endif

#endif // __BOARD_CONFIG_H__
