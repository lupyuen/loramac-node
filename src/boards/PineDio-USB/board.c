#include "board.h"
#include "gps.h"
#include "rtc-board.h"
#include "sx126x-board.h"
#include <stddef.h>
#include <stdio.h>

void BoardInitMcu( void ) {
  SX126xIoInit();
  SpiInit( &SX126x.Spi, SPI_1, 0, 0, 0, NC );
  RtcInit();
}

void BoardResetMcu( void ) {
  printf("BoardResetMcu()\n");
}

void BoardInitPeriph( void ) {
  printf("BoardInitPeriph()\n");

}

uint8_t BoardGetBatteryLevel( void ) {
  printf("BoardGetBatteryLevel()\n");

  return 0;
}

uint32_t BoardGetRandomSeed( void ) {
  printf("BoardGetRandomSeed()\n");

  return 0;
}

void BoardGetUniqueId( uint8_t *id ) {
  printf("BoardGetUniqueId()\n");

  id = NULL;
}

void BoardLowPowerHandler( void ) {
  // TODO ?
  //printf("BoardLowPowerHandler()\n");

}

void BoardCriticalSectionBegin( uint32_t *mask ) {
//  printf("BoardCriticalSectionBegin()\n");

  // TODO
}

void BoardCriticalSectionEnd( uint32_t *mask ) {
//  printf("BoardCriticalSectionEnd()\n");

  // TODO
}