#include "board.h"
#include "gpio.h"
#include "spi-board.h"
#include "utilities.h"
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/spi/spidev.h>
////TODO: We open "/dev/spidev1.0" instead of "/dev/spidev0.0"
const char *spiDriverOverrideFilePath ="/sys/class/spi_master/spi1/spi1.0/driver_override";////TODO
////Previously: const char *spiDriverOverrideFilePath ="/sys/class/spi_master/spi0/spi0.0/driver_override";
const char *spidevOverride = "spidev";

const char *spiBindFilePath = "/sys/bus/spi/drivers/spidev/bind";
const char *spiUnbindFilePath = "/sys/bus/spi/drivers/spidev/unbind";
const char *spiBind = "spi1.0";////TODO
////Previously: const char *spiBind = "spi0.0";
int handle;

void SpiInit( Spi_t *obj, SpiId_t spiId, PinNames mosi, PinNames miso, PinNames sclk, PinNames nss )
{
 CRITICAL_SECTION_BEGIN( );

 auto fd = open(spiDriverOverrideFilePath, O_WRONLY);
 if(fd == -1)
   printf("Cannot open SPI device (driver_override open error)");

 auto res = write(fd, spidevOverride, strlen(spidevOverride));
 if(res <= 0)
   printf("Cannot open SPI device (driver_override write error)");

 close(fd);

 fd = open(spiBindFilePath, O_WRONLY);
 if(fd == -1)
   printf("Cannot open SPI device (bind open error)");

 res = write(fd, spiBind, strlen(spiBind));
 if(res <= 0)
   printf("Cannot open SPI device (bind write error)");

 close(fd);

 handle = open("/dev/spidev1.0", O_RDWR);////TODO
 ////Previously: handle = open("/dev/spidev0.0", O_RDWR);
 if(handle == -1)
   printf("Cannot open SPI device (spidev open error)");

 uint8_t mmode = SPI_MODE_0;
 uint8_t lsb = 0;
 uint8_t bitsperword = 8;
 ioctl(handle, SPI_IOC_RD_BITS_PER_WORD, &bitsperword);
 ioctl(handle, SPI_IOC_WR_MODE, &mmode);
 ioctl(handle, SPI_IOC_WR_LSB_FIRST, &lsb);

  uint32_t speed = 1000000;
  ioctl(handle, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
  ioctl(handle, SPI_IOC_WR_MAX_SPEED_HZ, &speed);

 CRITICAL_SECTION_END( );
}

void SpiDeInit( Spi_t *obj )
{
  printf("SpiDeInit()\n");
  // TODO
}

void SpiFormat( Spi_t *obj, int8_t bits, int8_t cpol, int8_t cpha, int8_t slave )
{
  printf("SpiFormat()\n");
  // TODO

}

void SpiFrequency( Spi_t *obj, uint32_t hz )
{
  printf("SpiFrequency()\n");
  // TODO

}

uint16_t SpiInOut( Spi_t *obj, uint16_t outData )
{
  const uint8_t *mosi = &outData; // output data
  uint8_t buffer_in; // input data

  struct spi_ioc_transfer spi_trans;
  memset(&spi_trans, 0, sizeof(spi_trans));

  spi_trans.tx_buf = (unsigned long) mosi;
  spi_trans.rx_buf = (unsigned long) &buffer_in;
  spi_trans.cs_change = 0;
  spi_trans.len = 1;

  int res = ioctl(handle, SPI_IOC_MESSAGE(1), &spi_trans);
  if(res < 0) {
    printf("SpiInOut: IOCTL error (SPI_IOC_MESSAGE failed)\r\n");
    fflush(stdout);
  }

  return buffer_in;
}

uint16_t SpiInOutEnd() {
  const uint8_t buffer_out = 0;
  uint8_t buffer_in; // input data

  struct spi_ioc_transfer spi_trans;
  memset(&spi_trans, 0, sizeof(spi_trans));

  spi_trans.tx_buf = (unsigned long) &buffer_out;
  spi_trans.rx_buf = (unsigned long) &buffer_in;
  spi_trans.cs_change = 1;
  spi_trans.len = 1;

  int res = ioctl(handle, SPI_IOC_MESSAGE(1), &spi_trans);
  if(res < 0)
    printf("SpiInOutEnd: IOCTL error (SPI_IOC_MESSAGE failed)");

  return buffer_in;
}

////TODO Begin
////  We transfer SPI Data in Chunks instead of Byte-by-Byte.
////  Byte-by-Byte SPI Transfer seems unable to receive Join Accept Response.
////  But Chunked SPI Transfer causes Receive Timeout, which we work around in RadioIrqProcess.
#include <assert.h>
#include <stdbool.h>

/// Blocking call to transmit and receive buffers on SPI. Return 0 on success.
int transfer_spi(const uint8_t *tx_buf, uint8_t *rx_buf, uint16_t len) {
    assert(handle > 0);
    assert(len > 0);
    assert(len <= 31);  //  CAUTION: CH341 SPI doesn't seem to support 32-byte SPI transfers 

    //  Prepare SPI Transfer
    struct spi_ioc_transfer spi_trans;
    memset(&spi_trans, 0, sizeof(spi_trans));
    spi_trans.tx_buf = (unsigned long) tx_buf;  //  Transmit Buffer
    spi_trans.rx_buf = (unsigned long) rx_buf;  //  Receive Buffer
    spi_trans.cs_change = true;   //  Set SPI Chip Select to Low
    spi_trans.len       = len;    //  How many bytes
    //  printf("spi tx: "); for (int i = 0; i < len; i++) { printf("%02x ", tx_buf[i]); } printf("\n");

    //  Transfer and receive the SPI buffers
    int rc = ioctl(handle, SPI_IOC_MESSAGE(1), &spi_trans);
    assert(rc >= 0);
    assert(rc == len);

    //  printf("spi rx: "); for (int i = 0; i < len; i++) { printf("%02x ", rx_buf[i]); } printf("\n");
    return 0;
}
////TODO End