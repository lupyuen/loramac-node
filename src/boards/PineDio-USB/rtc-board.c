#include "rtc-board.h"
#include <bits/types/time_t.h>
#include <stdio.h>
#include <time.h>

uint64_t RtcGetAbsoluteNowUs() {
  long int ns;
  uint64_t all;
  time_t sec;
  struct timespec spec;

  clock_gettime(CLOCK_REALTIME, &spec);
  sec = spec.tv_sec;
  ns = spec.tv_nsec;

  all = (uint64_t) sec * 1000000000L + (uint64_t) ns;
  all = all / 1000L;
  return all;
}
uint64_t rtcOffset;

uint32_t RtcGetNowUs() {
  return RtcGetAbsoluteNowUs() - rtcOffset;
}

void RtcInit() {
  rtcOffset = RtcGetAbsoluteNowUs();
}

TimerTime_t RtcTick2Ms( uint32_t tick ) {
  return tick / 1000;
}

TimerTime_t RtcTempCompensation( TimerTime_t period, float temperature ) {
  return 0;
}

uint32_t RtcMs2Tick( TimerTime_t milliseconds ) {
  return milliseconds *  1000;
}

uint32_t RtcGetTimerValue( void ) {
  return RtcGetNowUs();
}

uint32_t RtcGetMinimumTimeout( void ) {
  return 1000;
}

uint64_t rtcAlarm;
uint64_t rtcAlarmSet;
uint64_t rtcTimerContext;

uint32_t RtcGetTimerElapsedTime( void ) {
  uint32_t elapsed = RtcGetNowUs() -  rtcTimerContext;
  return (elapsed);
}

void RtcStopAlarm( void ) {

}

uint64_t RtcGetAlarm() {
  return rtcAlarm;
}

void RtcSetAlarm( uint32_t timeout ) {
  rtcAlarmSet = RtcGetNowUs();
  rtcAlarm = rtcAlarmSet + timeout;
}


uint32_t RtcGetTimerContext( void ) {
  return rtcTimerContext;
}

uint32_t RtcSetTimerContext( void ) {
  rtcTimerContext = RtcGetNowUs();

  return rtcTimerContext;
}

void RtcBkupRead( uint32_t* data0, uint32_t* data1 ) {
  *data0 = RtcGetCalendarTime(data1);
}

uint32_t RtcGetCalendarTime( uint16_t *milliseconds ) {

  long int ns;
  uint64_t all;
  time_t sec;
  struct timespec spec;

  clock_gettime(CLOCK_REALTIME, &spec);
  sec = spec.tv_sec;
  ns = spec.tv_nsec;

  all = (uint64_t) sec * 1000000000L + (uint64_t) ns;

  *milliseconds = all/1000000;

  return all/1000000000;
}

void RtcBkupWrite( uint32_t data0, uint32_t data1 ) {

}